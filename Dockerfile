FROM alpine:latest as builder

ARG OPENRESTY_VERSION
ARG LUA_VERSION
ARG LUAROCKS_VERSION

RUN apk update
RUN apk add --no-cache --upgrade bash curl ncurses openssl
RUN apk add --update gcc g++ musl-dev make pcre pcre-dev openssl-dev zlib zlib-dev readline-dev perl openldap-dev
RUN apk add build-base

# Add packeges
ADD ./lua-${LUA_VERSION}.tar.gz /tmp/
ADD ./luarocks-${LUAROCKS_VERSION}.tar.gz /tmp/
ADD ./openresty-${OPENRESTY_VERSION}.tar.gz /tmp/
ADD ./nginx-auth-ldap-master.tar /tmp/

# Lua build
ADD ./scripts/build-lua /tmp/build-lua
RUN sh /tmp/build-lua

# Nginx build
ADD ./scripts/build-openresty /tmp/build-openresty
RUN sh /tmp/build-openresty

#############
# Final Image
#############

FROM node:14.7.0-alpine3.10
LABEL maintainer="Jamie Curnow <jc@jc21.com>"

# Env var for bashrc
ARG OPENRESTY_VERSION
ENV OPENRESTY_VERSION=${OPENRESTY_VERSION}

# OpenResty uses LuaJIT which has a dependency on GCC
RUN apk update \
	&& apk add gcc musl-dev curl bash figlet ncurses openssl pcre zlib apache2-utils tzdata perl readline unzip openldap-dev \
	&& apk add --update make \
	&& rm -rf /var/cache/apk/*

ADD ./.bashrc /root/.bashrc

# Copy lua and luarocks builds from first image
COPY --from=builder /tmp/lua /tmp/lua
COPY --from=builder /tmp/luarocks /tmp/luarocks
ADD ./scripts/install-lua /tmp/install-lua

# Copy openresty build from first image
COPY --from=builder /tmp/openresty /tmp/openresty
COPY --from=builder /tmp/nginx-auth-ldap-master /tmp/nginx-auth-ldap-master
ADD ./scripts/install-openresty /tmp/install-openresty

RUN sh /tmp/install-lua \
	&& sh /tmp/install-openresty \
	&& rm -f /tmp/install-lua \
	&& rm -f /tmp/install-openresty \
	&& mkdir -p /var/cache/nginx/client_temp \
	&& apk del make
